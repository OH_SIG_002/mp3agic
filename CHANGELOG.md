## v2.0.1

- 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)上验证通过

## v2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)
- 新增XTS用例
- ArkTs 语法适配

## 1.0.2

- 适配DevEco Studio 3.1Beta1版本。
- 适配OpenHarmony SDK API version 9版本。

## v1.0.1

- api8升级到api9
- 补充exception的逻辑

## v1.0.0

1. 读取低级 mpeg 帧数据
2. 读取、写入、添加和删除 ID3v1 和 ID3v2 标签（ID3v2.3 和 ID3v2.4）
3. 读取过时的 3 字母 ID3v2.2 标签（但不写入）
4. 通过查看实际的 mpeg 帧正确读取 VBR 文件
5. 读取和写入嵌入式图像（如专辑封面）
6. 在 mpeg 帧的结尾和 ID3v1 标签之间添加或删除自定义消息
7. Unicode 支持

- 未实现的功能如下：

不支持不同编码格式解码，例如构造使用"UTF-8"，解码使用“"ISO-8859-1"。