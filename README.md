# mp3agic

## Overview

mp3agic is a library used to read MP3 files and read/operate ID3 tags (ID3v1 and ID3v2.2 to ID3v2.4). It helps you to handle complex file operations.

## Display Effect

![gif](preview_EN.gif)

## How to Install

 ```
 ohpm install @ohos/mp3agic
 ```

For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

Multiple test redirection pages such as ID3v1NoTrackTest and ID3v1TagOrID3v2TagTest are provided. The **index** page is the main entry page.

Entry initialization: Click the corresponding button in the test button list to perform a test.

Step 1: Initialize **Mp3File**.
 ```
 let mp3file = new Mp3File(this.path)
 ```

Step 2: Obtain tags and data details.

 ```
 let id3v1Tag: ID3v1 = mp3file.getId3v1Tag();
 console.log('mp3agic Id3v1Tag Track: ' + id3v1Tag.getTrack());
 console.log('mp3agic Id3v1Tag Artist: ' + id3v1Tag.getArtist());
 ```

For details, see the implementation on the sample page of the open-source library.

## Test Method

1. Use cn.openharmony.mp3agic to sign the HAP. (You can customize the HAP name as required. If you do this, change HAP name in the script accordingly.)
2. After the HAP is installed, click to run the application. (If the application does not open, file pushing may have failed.)
3. Run the **pushMP3File.bat** script in **entry\src\main\resources\media** to push the used files to the specified directory.
4. Access the device via hdc shell, go to **/data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry**, and run **ls -al** to view the attributes of the **files** folder.
Change the attributes of all files in the **files** folder to be the same as those of the **files** folder. Reference command: chown 20010032:20010032 \*.*
5. After the modification is complete, click the corresponding button on the page and run **hilog | grep mp3agiclog** to filter logs.

## Notes

The test resource files of the sample are uploaded in git lfs mode. If you directly download the .zip package of the project, only the shortcuts of the obtained resource files are provided. You need to manually download the files from the resource file directory. Alternatively, download the code in git clone mode. In this way, the obtained resource files are complete.

## Available APIs

```
let mp3file = new Mp3File('xxxxxxpath');
```
1. mp3file.getId3v1Tag (): Obtains 3v1 tags.
2. mp3file.hasId3v1Tag (): Checks whether 3v1 tags exist.
3. mp3file.hasId3v2Tag (): Checks whether 3v2 tags exist.
4. mp3file.getFrameCount(): Obtains the number of frames in an MP3 file.
5. mp3file.getStartOffset (): Obtains the start offset.
6. mp3file.getEndOffset (): Obtains the end offset.
7. mp3file.getLengthInMilliseconds(): Obtains the length of the MP3 file, in milliseconds.
8. mp3file.getLengthInSeconds(): Obtains the length of the MP3 file, in seconds.
9. mp3file.isVbr(): Checks whether the encoding format is VBR instead of CBR.
10. mp3file.getBitrate(): Obtains the bit rate.
11. mp3file.getChannelMode(): Obtains the channel mode.
.......



## Constraints

mp3agic has been verified in the following versions:

- DevEco Studio Version: 4.1 Canary (4.1.3.317)

- OpenHarmony SDK: API11 (4.1.0.36)

## Directory Structure

```
|---- mp3agic  
|     |---- entry  # Sample code
|     |---- library  # mp3agic library
|          |---- src
|            |---- main
|              |---- ets
|                  |---- commonents
|                    |---- mp3agic   # Core code of mp3agic
|                       |---- Mp3File.ets   # MP3 file operation class
|                       |---- ID3v22Tag.ets   # Version 2.0 tags
|                       |---- ID3v23Tag.ets   # Version 3.0 tags
|                       |---- ID3v24Tag.ets   # Version 4.0 tags
|                       |---- ID3v2Frame.ets   # Audio packet data encapsulation
|                       |---- ID3v2TextFrameData.ets   # Text frame data
|                       |---- ID3v2ChapterFrameData.ets   # Chapter frame data
|                       ......
|           |---- index.ets  # External APIs
|     |---- README_EN.md  # Readme
|     |---- README_zh.md  # Readme
```

## How to Contribute
If you find any problem when using mp3agic, submit an [Issue](https://gitee.com/openharmony-sig/mp3agic/issues) or a [PR](https://gitee.com/openharmony-sig/mp3agic/pulls).

## License
This project is licensed under the terms of the [MIT License](https://gitee.com/openharmony-sig/mp3agic/blob/master/mit-license.txt).
