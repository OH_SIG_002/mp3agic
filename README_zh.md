# mp3agic

## 简介

mp3agic 用于读取 mp3 文件和读取/操作 ID3 标签（ID3v1 和 ID3v2.2 到 ID3v2.4）,协助开发者处理繁琐的文件操作相关，多用于操作文件场景的业务应用。

## 效果展示：

![gif](preview.gif)

## 下载安装

 ```
 ohpm install @ohos/mp3agic
 ```

OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

提供多种ID3v1NoTrackTest、ID3v1TagOrID3v2TagTest等测试跳转页面，index为主入口界面。

入口初始化：按钮测试列表，点击对应按钮进行测试。

第一步：初始化 Mp3File
 ```
 let mp3file = new Mp3File(this.path)
 ```

第二步：获取tag，并获取数据详情信息

 ```
 let id3v1Tag: ID3v1 = mp3file.getId3v1Tag();
 console.log('mp3agic Id3v1Tag Track: ' + id3v1Tag.getTrack());
 console.log('mp3agic Id3v1Tag Artist: ' + id3v1Tag.getArtist());
 ```

更多详细用法请参考开源库sample页面的实现

## 组件测试方法

1. hap签名使用cn.openharmony.mp3agic（可以依据自己定义，需要修改推送文件的脚本的hap名称与自定义一致即可）
2. 安装好hap，点击进入应用。（不进入有可能文件推送不成功）
3. 执行entry\src\main\resources\media下的pushMP3File.bat脚本会将使用到的文件推送到指定目录。
4. hdc shell 进入板端，然后进到该目录 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry，接着通过ls -al查看files文件夹的属性。
将files下的文件全部修改和files文件夹一致的属性。 参考命令：chown 20010032:20010032 \*.*
5. 修改完成即可点击页面点击对应按钮，通过命令过滤 查看日志（hilog | grep mp3agiclog）

## 注意事项：

sample内置的测试资源文件是通过git lfs的方式上传的，如果直接下载项目的zip包，得到的资源文件只是一个快捷方式，需要进资源文件目录手动下载。或者通过git clone的方式下载代码，这样得到的资源文件是完整的。

## 接口说明

```
let mp3file = new Mp3File('xxxxxxpath');
```
1. 获取3v1本版标签：mp3file.getId3v1Tag()
2. 判断是否有3v1本版标签：mp3file.hasId3v1Tag()
3. 判断是否有3v2本版标签：mp3file.hasId3v2Tag()
4. 获取MP3文件帧数：mp3file.getFrameCount()
5. 获取起始设置：mp3file.getStartOffset()
6. 获取结束设置：mp3file.getEndOffset()
7. 获取MP3长度，单位毫秒：mp3file.getLengthInMilliseconds()
8. 获取MP3长度，单位秒：mp3file.getLengthInSeconds()
9. 是否为VBR编码，不是为 CBR编码：mp3file.isVbr()
10. 获取码率：mp3file.getBitrate()
11. 获取渠道模式：mp3file.getChannelMode()
.......



## 约束与限制

在下述版本验证通过

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)
- DevEco Studio 版本： 4.1 Canary(4.1.3.317) OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构

```
|---- mp3agic  
|     |---- entry  # 示例代码文件夹
|     |---- library  # mp3agic库文件夹
|          |---- src
|            |---- main
|              |---- ets
|                  |---- commonents
|                    |---- mp3agic   # 核心库代码文件夹
|                       |---- Mp3File.ets   # mp3文件操作类
|                       |---- ID3v22Tag.ets   # 2.0标签
|                       |---- ID3v23Tag.ets   # 3.0版本标签
|                       |---- ID3v24Tag.ets   # 4.0版本标签
|                       |---- ID3v2Frame.ets   # 音频包数据封装
|                       |---- ID3v2TextFrameData.ets   # 文本数据
|                       |---- ID3v2ChapterFrameData.ets   # 章节帧数据
|                       ......
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法
|     |---- README_zh.md  # 安装使用方法
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/mp3agic/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/mp3agic/pulls) 。

## 开源协议
本项目基于 [MIT License ](https://gitee.com/openharmony-sig/mp3agic/blob/master/mit-license.txt) ，请自由地享受和参与开源。


